from setuptools import setup, find_packages

setup(name='Project 35y python environment',
      version='1.0',
      description='Different modules that composes the python environment of project 35y',
      long_description='Our very long explanation on what our package does',
      classifiers=[
        'Development Status :: early stage',
        'Programming Language :: Python :: 3',
      ],
      install_requires=['pytube', 'pandas', 'tk', 'spotipy'], # this will normally be containing dependencies from packages
      packages=find_packages(exclude=("doc",".git",".idea","venv", "test")),
      keywords='',
      author='Eternal_kid',
      author_email='',
      license='MIT')