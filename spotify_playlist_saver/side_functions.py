import pandas as pd

def get_full_playlist_tracks(sp, playlist_id):

    results = sp.playlist_items(playlist_id)
    tracks = results['items']
    while results['next']:
        results = sp.next(results)
        tracks.extend(results['items'])
    return tracks

def import_playlist_dataframe(sp):

    playlists = sp.current_user_playlists()

    res = []

    for playlist in playlists['items']:

        playlist_name = playlist['name']
        playlist_id = playlist['id']

        tracks = get_full_playlist_tracks(sp, playlist_id)

        for t in tracks:
            [track_id, track_name, album_name, artists_name] = [
                t['track']['id'],
                t['track']['name'],
                t['track']['album']['name'],
                [t['track']['artists'][i]['name'] for i in range(len(t['track']['artists']))]
            ]

            index = playlist_id+'_'+track_id

            res.append([playlist_name, track_name, album_name, artists_name, playlist_id, track_id])

    df = pd.DataFrame(res, columns=['playlist_name', 'track_name', 'track_album', 'track_artists', 'playlist_id','track_id'])

    return df