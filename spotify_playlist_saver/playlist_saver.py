import spotipy
from spotipy.oauth2 import SpotifyOAuth
import cred
from side_functions import import_playlist_dataframe
from datetime import datetime

#acessing the playlists

scope = "user-read-private"

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=cred.client_ID, client_secret=cred.client_SECRET,
                                               redirect_uri=cred.redirect_url, scope=scope))

# extraction of the playlist data

df = import_playlist_dataframe(sp)

# saving the data

time = datetime.now().strftime('%Y_%m_%d_%H_h_%M_min')

df.to_csv('spotify_playlist_status_'+time+'.csv')

# final message

playlist = df.groupby('playlist_name')

print('%s playlist has been sucessfully saved'%len(playlist))