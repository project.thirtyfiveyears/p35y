import pytest
from yt_negator.other_small_fonctions import string_converter_proper_file_name

def test_string_converter_proper_file_name():

    test1 = 'first test'

    assert string_converter_proper_file_name(test1) == 'first_test'

    with pytest.raises(AssertionError) as e:
        string_converter_proper_file_name(78)
    assert str(e.value) == 'The given input is not a string'

    test2 = 'öàéüî/?|<>/'

    assert string_converter_proper_file_name(test2) == 'oaeui___inf__sup__'



