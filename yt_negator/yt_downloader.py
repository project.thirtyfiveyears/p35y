import os
from pytube import YouTube
from yt_negator.other_small_fonctions import check_name_in_dir, string_converter_proper_file_name, get_number, get_path, choose_among_options, open_channel_dataframe, yn_question, select_df_from_channel_dataframe, get_link
from yt_negator.explore_channel import see_channel_video, interactive_explore_channel
from pytube import Channel

def youtube_audio_downloader(video_link, **save_args):
    """
    Downloading the audio from a youtube video link
    :param video_link: the link to the video to download
    :param save_args:
            - filename: string if you want to save the video under a specific name (default: video title)
            - save_path: string if you want to save in a specific folder (default cwd/downloads/audio)
            - create_copy: True if you want to download even if a file with the same name already exists (default False)
            - quality: '48kpbs'/'128kbps (default 128kbps)
    :return:
    """

    # Getting the video object
    yt = YouTube(video_link)

    filename = save_args.get('filename', f"{string_converter_proper_file_name(yt.title)}.mp4")
    save_path = get_path()+'\\audio'
    quality = save_args.get('audio_quality', '128kbps')

    if quality not in ['48kbps', '128kbps']:
        quality = '128kbps'

    test = check_name_in_dir(filename, save_path)

    if test:
        create_copy = yn_question('It looks like this audio has already been downloaded. Do you want to create a copy?')
        if create_copy == 'y':
            i = 1
            while check_name_in_dir(filename, save_path):
                filename = f"{filename}({i}).mp4"
                i += 1

            youtube_audio_downloader(video_link, filename=filename, **save_args)
        else:
            print('This file already exists')

    else:
        video_stream = yt.streams.filter(only_audio=True)

        for i in range(len(video_stream)):
            if video_stream[i].mime_type == 'audio/mp4' and video_stream[i].abr == quality:
                ind = i

        if ind:

            video_stream[ind].download(output_path=save_path, filename=filename)
            print(f'{yt.title} audio saved')

        else:
            print(f'Couldn\'t download this: {yt.title}.')
    return


def youtube_video_downloader(video_link, **save_args):
    """
    Downloading the video and audio from a youtube video link in a new folder
    :param video_link: the link to the video to download
    :param save_args:
            - dirname: string if you want to save the video in a folder with a specific name (default: video title)
            - save_path: string if you want to save in a specific folder (default cwd/downloads/video)
            - create_copy: True if you want to download even if a file with the same name already exists (default False)
            - quality: '48kpbs'/'128kbps (default 128kbps)
            - resolution: resolution of the video to download (default 720p)
    :return:
    """
    yt = YouTube(video_link)
    save_path = get_path()+'\\video'
    dirname = save_args.get('dirname', string_converter_proper_file_name(yt.title))

    test = check_name_in_dir(dirname, save_path)

    if test:
        create_copy = yn_question('It looks like this audio has already been downloaded. Do you want to create a copy?')

        if create_copy == 'y':
            i = 1
            while check_name_in_dir(dirname, save_path):
                dirname = f'{string_converter_proper_file_name(yt.title)}({i})'
                i += 1
            youtube_video_downloader(video_link, dirname=dirname, **save_args)

        else:
            print('This file already exists')

    else:

        save_path = save_path + '\\' + dirname

        os.makedirs(save_path)

        filename = f'{string_converter_proper_file_name(yt.title)} video.mp4'

        resolution = save_args.get('res', '720p')

        vids = yt.streams.filter(only_video=True)


        for i in range(len(vids)):
            if vids[i].mime_type == 'video/mp4' and vids[i].resolution == resolution:
                ind = i

        if ind:

            vids[ind].download(output_path=save_path, filename=filename)

            youtube_audio_downloader(video_link, save_path=save_path, filename=
            f'{string_converter_proper_file_name(yt.title)} audio.mp4')

            print(f'{yt.title} video saved')

        else:
            print(f'Couldn\'t download this: {yt.title}.')

    return


def interactive_downloader():


    print('---------- Youtube downloader main menu ----------')

    going_on = True

    while going_on:

        action = choose_among_options('Available actions:', ['download from the channel list', 'download from a link', 'exit'])

        if action == 1:

            channel_df = open_channel_dataframe()
            q3 = yn_question('Do you want to explore the channel dataframe first?')

            if q3 == 'y':
                interactive_explore_channel(channel_df)

            channel_id = select_df_from_channel_dataframe(channel_df)

            download_from_channel(channel_df.loc[channel_id, 'channel_link'])

            print('\n Back to the youtube downloader main menu:')
            print('---------- Youtube downloader main menu ----------')

        if action == 2:

            l = get_link(type='video')
            f = choose_among_options('What to download ?', ['only video audio', 'full video'])

            if f == 1:
                youtube_audio_downloader(l)

            if f == 2:
                youtube_video_downloader(l)

            print('\n Back to the youtube downloader main menu:')
            print('---------- Youtube downloader main menu ----------')

        if action == 3:
            going_on = False
            print('Closing the youtube downloader')


def download_from_channel(channel_link):

    channel = Channel(channel_link)

    vid_nr = len(channel.videos)

    q = yn_question('Do you know the number of the video to download?')

    if q == 'y':
        see_channel_video(channel_link)

    n = get_number((1, vid_nr), 'Video number to download')
    f = choose_among_options('What to download ?', ['only video audio', 'full video'])

    if f == 1:
        youtube_audio_downloader(channel.videos[n - 1].watch_url)

    if f == 2:
        youtube_video_downloader(channel.videos[n - 1].watch_url)

