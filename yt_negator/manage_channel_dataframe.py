from pytube import Channel

from yt_negator.other_small_fonctions import select_df_from_channel_dataframe, open_channel_dataframe, \
    save_channel_dataframe, get_link, display_channel_dataframe, get_channel_type, get_channel_label, \
    choose_among_options, get_video_from_link_list, yn_question

from yt_negator.yt_downloader import youtube_audio_downloader, youtube_video_downloader

def interactive_add_channel(print_available_channel=True):

    channel_df = open_channel_dataframe()

    if print_available_channel:
        display_channel_dataframe(channel_df)

    l = get_link(type='channel')

    ch = Channel(l)
    ch_id = ch.channel_id

    if ch_id not in channel_df.index:

        channel_type = get_channel_type()
        channel_label = get_channel_label(channel_df)
        channel_df.loc[ch_id, :] = [ch.channel_name, ch.channel_url, channel_type, channel_label, ch.videos[0].video_id]
        save_channel_dataframe(channel_df, msg=False)
        print('--> %s channel added !'%ch.channel_name)

    else:
        print('This channel is already in the list')

def interactive_remove_channel():
    channel_df = open_channel_dataframe()

    ch_id = select_df_from_channel_dataframe(channel_df)

    if ch_id in channel_df.index:

        channel_df = channel_df[channel_df.index != ch_id]
        print('--> Channel removed')
        save_channel_dataframe(channel_df, msg=False)

    else:
        print('This channel is not in channel dataframe')



def check_for_new_video(): #TODO: improve
    channel_df = open_channel_dataframe()

    q = yn_question('Do you want to only check serious channels and exclude entertainment channels?')

    if q == 'y':
        indexes = channel_df[channel_df['channel_type']=='serious'].index

    if q == 'n':
        indexes = channel_df.index

    total_new_videos = 0
    new_video_list = []

    for i in indexes:

        ch = Channel(channel_df.loc[i, 'channel_link'])

        test_new_video = True

        new_video = 0

        while test_new_video:

            if not ch.videos[new_video].video_id == channel_df.loc[i, 'latest_video']:
                new_video_list.append(ch.videos[new_video].watch_url)
                new_video += 1


            else:
                test_new_video = False

        if new_video != 0:
            print(f'{ch.channel_name} has {new_video} new video(s).')
            total_new_videos += new_video
            channel_df.loc[i, 'latest_video'] = ch.videos[0].video_id

    print(f'The check is over. {total_new_videos} new videos has ben found')

    if total_new_videos != 0:

        u = yn_question('Do you want to update the monitor channel list?')
        if u == 'y':
            save_channel_dataframe(channel_df)


        q = yn_question('Do you want to download a video?')

        while q == 'y':

            l = get_video_from_link_list(new_video_list)

            f = choose_among_options('What to download ?', ['only video audio', 'full video'])

            if f == 1:
                youtube_audio_downloader(l)

            if f == 2:
                youtube_video_downloader(l)

            q = yn_question('Do you want to download another video?')

    return new_video_list

def interactive_manage_channel():

    print('---------- Channel manager main menu ----------')

    going_on = True

    while going_on:
        action = choose_among_options('Available actions:',
                                      ['add a channel',
                                       'remove a channel',
                                       'exit'])

        if action ==1:
            interactive_add_channel()
            print('\n Back to the channel list manager main menu:')
            print('---------- Channel manager main menu ----------')

        if action == 2:
            interactive_remove_channel()
            print('\n Back to the channel list manager main menu:')
            print('---------- Channel manager main menu ----------')

        if action == 3:
            going_on = False


    print('Closing the channel list manager !')