import os
import pandas as pd
from yt_negator.manage_channel_dataframe import interactive_add_channel
from yt_negator.other_small_fonctions import yn_question


def youtube_negator_initialization():

    # 1. creating a folder to store the monitored channels and the downloads

    print('------------------------------------------')
    print('The youtube negator is a great way to avoid the time loss due to Youtube, '
          'while following your favorite channels!')
    print('It makes it possible to a list of channels that you want to follow')
    print('And to download videos (in the audio or video format (...')
    print('------------------------------------------')

    path = input('Please provide a path to the place where you want to download the videos '
              '(current directory if default): ')
    test = True
    while test:
        if path == 'default':
            path = os.getcwd()
        try:
            os.mkdir(path+'\\youtube_negator_files')
            test = False
        except:
            print('Path not valid')
            path = input('Please provide a path to the place where you want to download the videos: ')

    os.mkdir(path + '\\youtube_negator_files\\audio')
    os.mkdir(path + '\\youtube_negator_files\\video')

    with open(os.getcwd()+'\\path_directory.txt', 'w') as f:
        f.write(path + '\\youtube_negator_files')

    # 2. initializing the channel dataframe

    channel_df = pd.DataFrame(columns=['channel_name', 'channel_link', 'channel_type', 'channel_label', 'latest_video'])
    channel_df.to_csv(path_or_buf=(path + '\\youtube_negator_files\\channel_df.csv'), index_label='channel_id')

    # 3. adding the first channels

    q = yn_question('Do you want to add a channel ?')

    while q == 'y':
        interactive_add_channel()
        q = yn_question('Do you want to add another channel ?')

    print('Initialization complete !')
