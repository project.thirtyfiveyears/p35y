from yt_negator.other_small_fonctions import yt_negator_initialization_test, choose_among_options, yn_question
from yt_negator.explore_channel import interactive_explore_channel
from yt_negator.manage_channel_dataframe import interactive_manage_channel, check_for_new_video
from yt_negator.yt_downloader import interactive_downloader
going_on = True

print('Welcome in the youtube negator !')
print('---------- Youtube negator main menu ----------')


if not yt_negator_initialization_test():
    print('It seems like the youtube negator is not yet initialized. ')
    q = yn_question('Do you want to initialize it ?')

    if q == 'y':
        from yt_negator.setup_youtube_negator import youtube_negator_initialization
        youtube_negator_initialization()

    if q == 'n':
        going_on = False

while going_on:

    action = choose_among_options('Available actions:',
                                  ['explore a monitored channel',
                                   'manage the monitored channel list',
                                   'download a video',
                                   'check for new videos',
                                   'exit'])

    if action == 1:
        interactive_explore_channel()

    if action == 2:
        interactive_manage_channel()

    if action == 3:
        interactive_downloader()

    if action == 4:
        check_for_new_video()

    if action in [1, 2, 3, 4]:
        print('---------- Youtube negator main menu ----------')

    if action == 5:
        going_on = False


print('The youtube negator is going to close. See you soon!')
q = input('type anything to close end: ')
