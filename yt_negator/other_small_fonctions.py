import string
import os
import pandas as pd
from pytube import YouTube, Channel


def display_channel_dataframe(channel_df):
    print('List of monitored channels:')
    for i in range(len(channel_df.index)):
        n = channel_df.loc[channel_df.index[i], 'channel_name']
        print(f'{i + 1}. {n}')


def select_df_from_channel_dataframe(channel_df):
    nb_chaines = len(channel_df.index)

    print('List of monitored channels:')
    for i in range(len(channel_df.index)):
        n = channel_df.loc[channel_df.index[i], 'channel_name']
        print(f'{i + 1}. {n}')

    q2 = input('Select a channel (number between 1 and %s): \n -->' % nb_chaines)
    while q2 not in [str(i) for i in range(1, nb_chaines + 1)]:
        print('Erreur')
        q2 = input('Select a channel: \n -->')

    channel_id = channel_df.index[int(q2) - 1]

    return channel_id

def get_number(number_range, get_msg):
    n = input(f'{get_msg} (between {number_range[0]} and {number_range[1]}): \n -->')
    while n not in [str(i) for i in range(number_range[0], number_range[1] + 1)]:
        print('Erreur')
        n = input(f'{get_msg} (between {number_range[0]} and {number_range[1]}): \n -->')

    return int(n)

def yn_question(get_msg):
    q = input(f'{get_msg} (y/n): \n -->')
    while q not in ['y', 'n']:
        print('Erreur')
        q = input(f'{get_msg} (y/n): \n -->')

    return q

def get_path():
    with open(os.getcwd() + '\\path_directory.txt', 'r') as f:
        path = f.readline()
    return path

def yt_negator_initialization_test():
    initialization = True

    try:
        open(os.getcwd() + '\\path_directory.txt', 'r')
    except:
        initialization = False

    return initialization

def open_channel_dataframe():
    path = get_path()
    channel_df = pd.read_csv(path + '\\channel_df.csv', index_col='channel_id')

    return channel_df


def save_channel_dataframe(channel_df, msg=True):
    with open(os.getcwd() + '\\path_directory.txt', 'r') as f:
        path = f.readline()

    channel_df.to_csv(path + '\\channel_df.csv', index_label='channel_id')

    if msg:
        print('--> Channel dataframe successfully updated')


def choose_among_options(get_msg, option_list):

    print(get_msg)
    for i in range(len(option_list)):
        print(f'{i+1}. {option_list[i]}')

    q = input('Select a number: \n -->')
    while q not in [str(i) for i in range(1, len(option_list)+1)]:
        print('Error')
        q = input('Select a number: \n -->')

    return int(q)


def get_link(type='video'):

    if type not in ['video','channel']:
        raise ValueError('This is not a valid link type. Choose between video and channel')

    link = input('Please provide a %s link: \n -->'%type)
    test = True
    while test:
        try:
            if type == 'video':
                v = YouTube(link)
            if type == 'channel':
                c = Channel(link)
            test = False
        except:
            print('This link is not a valid %s link'%type)
            link = input('Please provide a %s link: \n -->'%type)
    if type == 'video':
        print(f'{v.title} selected')
    if type == 'channel':
        print(f'{c.channel_name} selected')

    return link

def get_video_from_link_list(video_link_list):
    video_title_list = []

    for l in video_link_list:
        video_title_list.append(YouTube(l).title)
    print(video_title_list)
    return video_link_list[choose_among_options('Video list:', video_title_list)-1]


def get_channel_type():
    channel_type = ['serious', 'entertainment']
    i = input('Please select a channel type: \n 1. serious \n 2. entertainment \n --> : ')
    while i not in ['1', '2']:
        print('This is not a valid channel type')
        i = input('Please select a channel type: \n 1. serious \n 2. entertainment \n --> : ')
    return channel_type[int(i)-1]

def get_channel_label(channel_df):

    try:
        label_list = list(set(channel_df['channel_label']))
    except:
        label_list = None

    channel_type = input('Please type a channel label (existing labels: %s): \n -->'%label_list)

    return channel_type

def string_converter_proper_file_name(string_chain):
    """
    Converting a string so that he can be used as a file or folder name
    :param string_chain: a string
    :return: a string properly formatted
    """

    # 1. replacing special characters to basic ones

    assert isinstance(string_chain, str), 'The given input is not a string'

    new_string = ''

    for s in fr'{string_chain}':
        if s in ' /*|':
            new_string += '_'
        elif s == '\\':
            new_string += '_'
        elif s in '?"':
            new_string += ''
        elif s == ':':
            new_string += '='
        elif s in 'êéèë':
            new_string += 'e'
        elif s in 'äàãáâ':
            new_string += 'a'
        elif s in 'îíìï':
            new_string += 'i'
        elif s in 'ôòóõö':
            new_string += 'o'
        elif s in 'ûùúü':
            new_string += 'u'
        elif s == '<':
            new_string += '_inf_'
        elif s == '>':
            new_string += '_sup_'
        else:
            new_string += s

    # 2. suppressing undesirable characters

    valid_chars = "=-_.() %s%s" % (string.ascii_letters, string.digits)

    final_string = ''.join(c for c in new_string if c in valid_chars)

    return final_string


def check_name_in_dir(name, dir_path):
    assert isinstance(name, str), 'Please provide name in a string format'
    assert isinstance(dir_path, str), 'Please provide path in a string format'
    file_list = os.listdir(dir_path)

    if name in file_list:
        return True

    else:
        return False
