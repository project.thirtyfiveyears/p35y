from pytube import Channel
from yt_negator.other_small_fonctions import open_channel_dataframe, display_channel_dataframe, get_number, yn_question


def see_channel_video(channel_link):
    channel = Channel(channel_link)

    vid_nr = len(channel.videos)

    print('------------------------------------------')
    print('Channel: %s' % channel.channel_name)

    n = get_number((1, vid_nr), 'How many video you want to see ?')

    for i in range(n):
        print(f'{i + 1}. {channel.videos[i].title}')

    if n == vid_nr:
        print('No more videos to see')
        q3 = 'n'
    else:
        q3 = yn_question('Do you want to see more ?')

    while q3 == 'y':

        n2 = get_number((1, vid_nr - n), 'How many more video do you want to see ?')
        for i in range(n, n2 + n):
            print(f'{i + 1}. {channel.videos[i].title}')
        n += n2

        if n == vid_nr:
            print('No more videos to see')
            q3 = 'n'
        else:
            q3 = yn_question('Do you want to see more ?')


def interactive_explore_channel():
    channel_df = open_channel_dataframe()



    q = yn_question('Do you want to only check serious channels and exclude entertainment channels?')

    if q == 'y':
        indexes = channel_df[channel_df['channel_type'] == 'serious'].index

    if q == 'n':
        indexes = channel_df.index

    nb_channel = len(indexes)

    print('---------- Channel explorer main menu ----------')
    display_channel_dataframe(channel_df.loc[indexes, :])

    q = yn_question('Do you want to see the videos from a channel?')

    while q == 'y':

        q2 = get_number((1, nb_channel), 'Select a channel number')
        see_channel_video(channel_df.loc[indexes, :].loc[channel_df.loc[indexes, :].index[q2-1], 'channel_link'])

        print('------------------------------------------')
        q = yn_question('Do you want to see the videos from another channel?')

        if q == 'y':
            display_channel_dataframe(channel_df.loc[indexes, :])

    print('Closing the channel explorer')

